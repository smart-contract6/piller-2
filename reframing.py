import google.generativeai as genai
from dotenv import load_dotenv
import os

load_dotenv()

GEMINI_API_KEY = os.getenv('GOOGLE_API_KEY')
working_model = genai.GenerativeModel(os.getenv('AI_MODEL'))
genai.configure(api_key=GEMINI_API_KEY)

def model_function(answer, question):
    context = "Given the provided answer and question, frame a clear and concise response. Ensure that the response properly addresses the question and follows logical sentence structure. Avoid unnecessary information.Do not use extra information from your knowledge."

    prompt = f"{context} Answer: {answer.strip()} Question: {question.strip()}. Now, rephrase the answer in a more structured and concise manner without any repeatetive content and without mentioning question in answer."

    response = working_model.generate_content(prompt, generation_config={
        "max_output_tokens": 2048,
        "temperature": 0,
        "top_p": 1
    })
    return response.text
