from database_connection import connect_to_database

def save_response_to_database( question_id,ref_id,final_response, doc_id, doc_Loc):
    try:
        connection = connect_to_database()
    
        
        cursor = connection.cursor()
        
        cursor.execute("INSERT INTO reference(refid,docid,docLoc,quesID,answer) VALUES (%s, %s, %s, %s, %s)", (ref_id,doc_id, doc_Loc,question_id,final_response,))
        connection.commit()
        
        print("Response saved to database successfully.")
    except Exception as e:
        print(f"Error occurred while saving response to database: {e}")
        connection.rollback()
    finally:
        cursor.close()


