import pandas as pd
import json
import os


def xlsx_to_json(excel_file,output_dir):
    print("3")
    xls = pd.ExcelFile(excel_file)
    # if not os.path.exists(output_dir):
    #     os.makedirs(output_dir)
    print("4")
    for sheet_name in xls.sheet_names:
        df = pd.read_excel(xls, sheet_name, header=None) 
        start_row = 0
        print("5")
        for i in range(len(df)):
            if not df.iloc[i].isnull().all():
                start_row = i
                break
        print("6")
        df = pd.read_excel(xls, sheet_name, header=start_row)
        data_dict = df.to_dict(orient='records')
        print("7")
        json_file = os.path.join(output_dir, f"{sheet_name}.json")
        with open(json_file, 'w') as f:
            print("8")
            json.dump(data_dict, f, indent=4)