from flask import Flask, request, jsonify, session
import psycopg2
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity


def check_credentials(username, passwordt):
    connection = None  
    cursor = None  

    try:
        connection = psycopg2.connect(
            dbname="pillar2",
            user="AutoGenAi",
            password="Freepassword12@",
            host="genaiauto.postgres.database.azure.com"
        )
        print(connection)
        cursor = connection.cursor()


        cursor.execute("SELECT password, del_flag FROM login WHERE user_id = %s", (username,))
        row = cursor.fetchone()

        if row:
            password, del_flag = row
            if passwordt == password:
                if del_flag:
                    return "disabled userid"
                else:
                    access_token = create_access_token(identity=username)
                    session['access_token'] = access_token
                    return jsonify(access_token=access_token), 200
            else:
                return jsonify({'message': 'Invalid credentials'}), 401
        else:
            return jsonify({'message': 'Invalid credentials'}), 401

    except psycopg2.OperationalError as op_error:
        print("OperationalError while connecting to PostgreSQL:", op_error)
        return "error"

    except psycopg2.Error as error:
        print("Error while connecting to PostgreSQL:", error)
        return "error"

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()






