import os
import json

def concatenate_json_files(directory):
    # Initialize an empty list to hold all the data
    all_data = []

    # Loop through each file in the directory
    for filename in os.listdir(directory):
        # Check if the file is a JSON file
        if filename.endswith('.json'):
            # Construct the full file path
            file_path = os.path.join(directory, filename)

            # Open the file and load the JSON data
            with open(file_path, 'r') as f:
                data = json.load(f)

            # Add the data to the list
            all_data.append(data)

    # Return the concatenated data
    return all_data

