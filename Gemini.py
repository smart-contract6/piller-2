from langchain_google_genai import GoogleGenerativeAIEmbeddings, ChatGoogleGenerativeAI
import google.generativeai as genai
from langchain_community.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.prompts import PromptTemplate
from dotenv import load_dotenv
import os

load_dotenv()


GEMINI_API_KEY = os.getenv('GOOGLE_API_KEY')
working_model = genai.GenerativeModel(os.getenv('AI_MODEL'))
genai.configure(api_key=GEMINI_API_KEY)



# async def answer_question(user_question, text_chunks):
#     # print("text_chunks",text_chunks)
#     embeddings = GoogleGenerativeAIEmbeddings(model= os.getenv('EMBEDDING_MODEL'))
#     new_db = FAISS.load_local("faiss_index", embeddings)
#     response = ""
#     docs =  new_db.similarity_search(user_question)
#     # print(docs)
#     text = ""
#     with open('Dictionary/synonymous.txt', 'r') as file:
#         text = file.read()

#     if docs:
#         prompt = f"""
#          Answer the Question depending on the given Context only.

               
         
#             Instructions:
#             1. Read the context and question carefully.
#             2. Answer the question based on the given context.
#             3. Do not add any new information.
#             4. Do not change the context.
#             5. Find the most relevant answer.
#             6. Find the answer that is most similar to the question.
#             7. If any of data you find in unicode then use your knowledge to decode it.


#          Context:  {docs}
#          Additional Instructions: {text}
#          Question: {user_question} """

#         # print("Prompt", prompt)
#         response = working_model.generate_content(prompt, generation_config={
#                 "max_output_tokens": 2048,
#                 "temperature": 0.5,
#                 "top_p": 1
#             })
#         # print("response.text",response.text)    
#         return response.text
#     else:
#         response = "No relevant context found."

#         # print("Answer finding...",response_data)
#     return response




async def answer_question(user_question, instructions):
    # print("text_chunks",text_chunks)
    embeddings = GoogleGenerativeAIEmbeddings(model= os.getenv('EMBEDDING_MODEL'))
    new_db = FAISS.load_local("faiss_index", embeddings)
    response = ""
    docs =  new_db.similarity_search(user_question)
    # print(docs)
    text = ""
    # with open('Dictionary/synonymous.txt', 'r') as file:
    #     text = file.read()

    # Additional Instructions: {text}

    if docs:
        prompt = f"""
         Answer the Question depending on the given Context only. Strictly follow all the instructions provided to answer the question.

         instructions: {instructions}
         Context:  {docs}
         Question: {user_question} """

        # print("Prompt", prompt)
        response = working_model.generate_content(prompt, generation_config={
                "max_output_tokens": 2048,
                "temperature": 0.5,
                "top_p": 1
            })
        # print("response.text",response.text)    
        print(response)
        return response.text
    else:
        response = "No relevant context found."

        # print("Answer finding...",response_data)
    return response

