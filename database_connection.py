import psycopg2

def connect_to_database():
    try:
        connection = psycopg2.connect(
            dbname="pillar2",
            user="AutoGenAi",
            password="Freepassword12@",
            host="genaiauto.postgres.database.azure.com"
        )
        return connection
    except psycopg2.Error as e:
        print("Error connecting to the database:", e)
        return None
    
def fetch_question_data(question_text, cursor):
    try:
        query = """SELECT sq.subquestion_text, i.instruction_text 
                   FROM questions q
                   JOIN subquestions sq ON q.question_id = sq.question_id
                   JOIN instructions i ON sq.subquestion_id = i.subquestion_id
                   WHERE q.question_text = %s
                   ORDER BY sq.subquestion_id"""  
        cursor.execute(query, (question_text,))
        question_data = cursor.fetchall()
        # print(question_data)
        return question_data
    except psycopg2.Error as e:
        print("Error fetching question data from the database:", e)
        return None