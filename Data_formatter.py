def arrange_data(tabular_data):
    # Initialize an empty dictionary to hold the arranged data
    arranged_data = {}

    # Loop through each item in the tabular data
    for item in tabular_data:
        # Loop through each key-value pair in the item
        for key, value in item.items():
            # Get the row and column indices from the first element of the value
            row, col = value[0]

            # If the row index is not already a key in the arranged data, add it
            if row not in arranged_data:
                arranged_data[row] = {}

            # Add the value to the appropriate row and column in the arranged data
            arranged_data[row][col] = value[1]

    # Convert the arranged data into a list of dictionaries (one for each row)
    arranged_data = [arranged_data[key] for key in sorted(arranged_data.keys())]

    return arranged_data

# Usage
tabular_data = [
    {
        "0": [[0, 0], "Date 15.12.2022"],
        "1": [[0, 1], "Invoice number 222000320"],
        "2": [[1, 0], "Due date 13.2.2023"],
        "3": [[1, 1], "Reference number 18 50401 47372"],
        "4": [[2, 0], "Your reference 4300000085"],
        "5": [[2, 1], "Our reference 222000320"],
        "6": [[3, 0], "Customer's business / VAT ID 29AADCO3578R1ZH"],
        "7": [[3, 1], "Customer number 15345581"],
        "8": [[4, 0], "Seller"],
        "9": [[4, 1], "Penalty interest 1,5 %"],
        "10": [[5, 0], "Delivery date 30.11.2022"],
        "11": [[5, 1], "Complaint time 8 days"],
        "12": [[6, 0], "Terms of payment 60 Days Net"],
        "13": [[7, 0], "Terms of delivery"],
        "14": [[7, 1], ""]
    }
]

print(arrange_data(tabular_data))