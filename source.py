import os
import json
from PyPDF2 import PdfReader
from collections import defaultdict
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


def extract_text_from_json(data):
    text = ""
    if isinstance(data, str):
        text += data + " "
    elif isinstance(data, dict):
        for value in data.values():
            text += extract_text_from_json(value)
    elif isinstance(data, list):
        for item in data:
            text += extract_text_from_json(item)
    return text


# def search_text_in_pdfs(input_text, json_folder):
#     scores = defaultdict(int)
#     input_words = input_text.lower().split()
    
#     for filename in os.listdir(json_folder):
#         if filename.endswith('.json'):
#             filepath = os.path.join(json_folder, filename)
#             with open(filepath, 'r') as f:
#                 json_data = json.load(f)
#                 text = extract_text_from_json(json_data).lower()
#                 for word in input_words:
#                     if word in text:
#                         scores[filename[:-5] + '.pdf'] += 1 
    
#     if not scores:
#         return("No Source found.")
#     else:
#         max_score_pdf = max(scores, key=scores.get)
#         return(f"{max_score_pdf}")
    

def search_text_in_pdfs(input_text, json_folder, original_folder):
    scores = defaultdict(int)
    input_words = input_text.lower().split()
    
    for filename in os.listdir(json_folder):
        if filename.endswith('.json'):
            filepath = os.path.join(json_folder, filename)
            with open(filepath, 'r') as f:
                json_data = json.load(f)
                text = extract_text_from_json(json_data).lower()
                for word in input_words:
                    if word in text:
                        pdf_filename = filename[:-5] + '.pdf'
                        actual_extension = '.xlsx'  
                        if os.path.isfile(os.path.join(original_folder, pdf_filename)):
                            actual_extension = os.path.splitext(pdf_filename)[1]
                        actual_filename = pdf_filename[:-4] + actual_extension
                        scores[actual_filename] += 1
    
    if not scores:
        return "No Source found."
    else:
        max_score_pdf = max(scores, key=scores.get)
        return max_score_pdf


# def find_text_in_pdf(pdf_file, text):
#     with open(pdf_file, 'rb') as f:
#         pdf_reader = PdfReader(f)
#         pdf_text = []
#         for page in pdf_reader.pages:
#             pdf_text.append(page.extract_text())

#         vectorizer = TfidfVectorizer()
#         tfidf_matrix = vectorizer.fit_transform(pdf_text)

#         text_tfidf = vectorizer.transform([text])
#         similarities = cosine_similarity(text_tfidf, tfidf_matrix)

#         max_similarity_page = similarities.argmax(axis=1)[0] + 1 
#         max_similarity_score = similarities[0, max_similarity_page - 1]
#         if max_similarity_score > 0.1:
#             return max_similarity_page
#     return None
    
def find_text_in_pdf(pdf_file, text):
    with open(pdf_file, 'rb') as f:
        pdf_reader = PdfReader(f)
        vectorizer = TfidfVectorizer()

        max_similarity_page = None
        max_similarity_score = 0

        for page_number, page in enumerate(pdf_reader.pages):
            page_text = page.extract_text()
            tfidf_matrix = vectorizer.fit_transform([page_text, text])
            similarities = cosine_similarity(tfidf_matrix)
            similarity_score = similarities[0, 1]

            if similarity_score > max_similarity_score:
                max_similarity_page = page_number
                max_similarity_score = similarity_score

        return max_similarity_page
