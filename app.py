import os
from flask import Flask, request, jsonify
import json
from Utils import concatenate_json_files
from Pdf_utils import read_pdf, search_text_in_pdfs, find_text_in_pdf, process_file, read_files_from_folder
from Embedding import get_text_chunks, get_vector_store, delete_index_from_vector_store
from Gemini import answer_question
from source import search_text_in_pdfs
from FileReader import process_files
from reframing import model_function
import asyncio
from dotenv import load_dotenv
from database_connection import connect_to_database, fetch_question_data
from response_to_db import save_response_to_database
# from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from authentication import check_credentials
import shutil
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_google_genai import GoogleGenerativeAIEmbeddings
import google.generativeai as genai
from langchain_community.vectorstores import FAISS

app = Flask(__name__) 
load_dotenv()

@app.route('/', methods=['GET'])
def home():
    return "App is running..."
   
  
 
@app.route('/ask', methods=['POST'])
async def ask_question():

    file = request.files['file']
    file.save(os.path.join('input', file.filename))

    payload_data = request.form.get('data')
    data = json.loads(payload_data)

    ref_id = data.get('data', {}).get('refid')
    docs = data.get('data', {}).get('docs', [])
    doc_id=docs.get('docid')
    doc_Loc=docs.get('docLoc')
    questions = data.get('data', {}).get('questions', [])

    # print(ref_id, docs, doc_id, doc_Loc, questions)

    input_dir = os.getenv('FILE_INPUT_DIRICTORY')
    output_dir = os.getenv('FILE_TO_JSON_OUTPUT_DIRICTORY')
    url = os.getenv('FILE_EXTRACTOR_URL')


    files = os.listdir(output_dir)
    if not files:
        loop = asyncio.get_event_loop()
        await process_files(input_dir, output_dir, url)
    print("File extraction finished...") 


    pdf_folder=os.getenv('FILE_INPUT_DIRICTORY')
    text = read_files_from_folder(pdf_folder)
    directory = os.getenv('FILE_TO_JSON_OUTPUT_DIRICTORY')
    text = json.dumps(concatenate_json_files(directory))

  
    text_chunks = get_text_chunks(text)

    print("Getting text chunk...")
    db = get_vector_store(text_chunks)

    print("Embedding...",db)
    responses = []
    connection = connect_to_database()
    if connection is None:
        return
    
    cursor = connection.cursor()
    print("!")
    for question_data in questions:
        question_text = question_data.get('question')
        print(question_text)
        print("1",question_data)
        question_id = question_data.get('question_id')
        question_info = fetch_question_data(question_text, cursor)

        print("questioninfo",question_info)
        
        if question_info:
            # print("Question:", question_text)
            question_response = ""
            
            for index, (subquestion, instruction) in enumerate(question_info, start=1):
                print(f"Subquestion {index}: {subquestion}")
                print(f"Instruction for Subquestion {index}: {instruction}")
                
                response = await answer_question(subquestion, instruction)
                print("1",response)
                final_response=model_function(response,question_text)
                print("2",final_response)
                question_response += final_response + "\n"
                save_response_to_database(question_id,ref_id,question_response,doc_id,doc_Loc)

                source= doc_Loc

            response = {
                "question_id": question_id,
                "question": question_text,
                "answer": question_response,
                "Reference":{
                    "source": source,
                #     "Page No.":"1"
                    }
                }
        responses.append(response)

    # shutil.rmtree('faiss_index')
    return jsonify(responses) 


@app.route('/record', methods=['POST']) 
def get_data_using_reference():

    data=request.get_json()
    refid=data.get('refid')

    connection = connect_to_database()
    if connection is None:
        return
    
    cursor = connection.cursor()

    
    

    
if __name__ == "__main__":
    app.run()